namespace DirecaoCerta.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pessoas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmpresaId = c.Int(nullable: false),
                        AuditoriaId = c.Int(nullable: false),
                        Email = c.String(),
                        Nome = c.String(),
                        EnderecoId = c.Int(),
                        CPF = c.String(),
                        Data_Aceite = c.DateTime(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Endereco_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Empresas", t => t.EmpresaId, cascadeDelete: true)
                .ForeignKey("dbo.Enderecoes", t => t.Endereco_Id)
                .Index(t => t.EmpresaId)
                .Index(t => t.Endereco_Id);
            
            CreateTable(
                "dbo.Empresas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RazaoSocial = c.String(),
                        CNPJ = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Enderecoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Logradouro = c.String(),
                        Cidade = c.String(),
                        Bairro = c.String(),
                        CEP = c.String(),
                        Numero = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Pessoas", new[] { "Endereco_Id" });
            DropIndex("dbo.Pessoas", new[] { "EmpresaId" });
            DropForeignKey("dbo.Pessoas", "Endereco_Id", "dbo.Enderecoes");
            DropForeignKey("dbo.Pessoas", "EmpresaId", "dbo.Empresas");
            DropTable("dbo.Enderecoes");
            DropTable("dbo.Empresas");
            DropTable("dbo.Pessoas");
        }
    }
}
