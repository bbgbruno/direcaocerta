﻿using DirecaoCerta.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DirecaoCerta.Controllers
{
    public class HomeController : AplicationController
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Menu()
        {
            Empresa empresaLogada = EmpresaSessao();
            if (empresaLogada != null)
                ViewBag.Message = "Nome Da Empresa " + empresaLogada.RazaoSocial;
            else
                ViewBag.Message = "Nenhum empresa logada";
            return View();
        }

    }
}
