﻿using ActionMailer.Net.Mvc;
using DirecaoCerta.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DirecaoCerta.Controllers
{
    public class MailController : MailerBase
    {
        public EmailResult CadastroNovaEmpresa(Empresa empresa, string senha, string mensagem)
        {
            To.Add(empresa.Email);
            From = "noreply.direcaocerta@gmail.com";
            Subject = String.Format("Direção certa - Empresa cadastrada por {0}", empresa.Responsavel);

            ViewBag.RazaoSocial = empresa.RazaoSocial;
            ViewBag.Responsavel = empresa.Responsavel;
            ViewBag.Usuario = empresa.Usuario;
            ViewBag.Email = empresa.Email;
            ViewBag.Senha = senha;
            ViewBag.Mensagem = mensagem;

            return Email("CadastroNovaEmpresa");
        }

        public EmailResult EmailContatoComercial(string nome, string email, string mensagem)
        {
            To.Add("noreply.direcaocerta@gmail.com");
            From = "noreply.direcaocerta@gmail.com";
            Subject = String.Format("Direção certa - Contato comercial de {0}", nome);

            ViewBag.Nome = nome;
            ViewBag.Email = email;
            ViewBag.Mensagem = mensagem;

            return Email("EmailContatoComercial");
        }
    }
}
