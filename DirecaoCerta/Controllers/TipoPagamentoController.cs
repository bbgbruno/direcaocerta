﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DirecaoCerta.Filters;
using DirecaoCerta.Models;
using WebMatrix.WebData;

namespace DirecaoCerta.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class TipoPagamentoController : AplicationController
    {
        private ContextoDeDados db = new ContextoDeDados();

        //
        // GET: /TipoPagamento/

        public ActionResult Index()
        {
            var tipopagamentos = db.TipoPagamentos.Include(t => t.Empresa).Include(t => t.Auditoria);
            return View(tipopagamentos.ToList());
        }

        //
        // GET: /TipoPagamento/Details/5

        public ActionResult Details(int id = 0)
        {
            TipoPagamento tipopagamento = db.TipoPagamentos.Find(id);
            if (tipopagamento == null)
            {
                return HttpNotFound();
            }
            return View(tipopagamento);
        }

        //
        // GET: /TipoPagamento/Create

        public ActionResult Create()
        {
            ViewBag.EmpresaId = new SelectList(db.Empresas, "Id", "RazaoSocial");
            ViewBag.AuditoriaId = new SelectList(db.Auditoria, "Id", "UltimaOperacao");
            return View();
        }

        //
        // POST: /TipoPagamento/Create

        [HttpPost]
        public ActionResult Create(TipoPagamento tipopagamento)
        {
           
            
            if (ModelState.IsValid)
            {
                Auditoria auditoria = new Auditoria();
                auditoria.DataAtualizacao = DateTime.Today;
                auditoria.DataCadastro = DateTime.Today;
                auditoria.UserId = WebSecurity.CurrentUserId;
                auditoria.UltimaOperacao = "Inclusao";
                tipopagamento.Auditoria = auditoria;
                tipopagamento.EmpresaId = IdEmpresaLogada();
                db.TipoPagamentos.Add(tipopagamento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipopagamento);
        }

        //
        // GET: /TipoPagamento/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TipoPagamento tipopagamento = db.TipoPagamentos.Find(id);
            if (tipopagamento == null)
            {
                return HttpNotFound();
            }
            ViewBag.EmpresaId = new SelectList(db.Empresas, "Id", "RazaoSocial", tipopagamento.EmpresaId);
            ViewBag.AuditoriaId = new SelectList(db.Auditoria, "Id", "UltimaOperacao", tipopagamento.AuditoriaId);
            return View(tipopagamento);
        }

        //
        // POST: /TipoPagamento/Edit/5

        [HttpPost]
        public ActionResult Edit(TipoPagamento tipopagamento)
        {
            
            if (ModelState.IsValid)
            {                
                tipopagamento.Auditoria = db.Auditoria.Find(tipopagamento.AuditoriaId);
                tipopagamento.Auditoria.UltimaOperacao = "Alteração";
                db.Entry(tipopagamento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EmpresaId = new SelectList(db.Empresas, "Id", "RazaoSocial", tipopagamento.EmpresaId);
            ViewBag.AuditoriaId = new SelectList(db.Auditoria, "Id", "UltimaOperacao", tipopagamento.AuditoriaId);
            return View(tipopagamento);
        }

        //
        // GET: /TipoPagamento/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TipoPagamento tipopagamento = db.TipoPagamentos.Find(id);
            if (tipopagamento == null)
            {
                return HttpNotFound();
            }
            return View(tipopagamento);
        }

        //
        // POST: /TipoPagamento/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoPagamento tipopagamento = db.TipoPagamentos.Find(id);
            db.TipoPagamentos.Remove(tipopagamento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}