﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DirecaoCerta.Filters;
using DirecaoCerta.Models;
using WebMatrix.WebData;

namespace DirecaoCerta.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class ServicoController : AplicationController
    {
        private ContextoDeDados db = new ContextoDeDados();
        public int idEmpresa;

        //
        // GET: /Servico/

        public ActionResult Index()
        {
            int idEmpresa = IdEmpresaLogada();
            var servicos = db.Servicos.Include(s => s.Empresa).Include(s => s.Auditoria).Where(d => d.EmpresaId == idEmpresa);
            return View(servicos.ToList());
        }
        
        //
        // GET: /Servico/Details/5

        public ActionResult Details(int id = 0)
        {
            Servico servico = db.Servicos.Find(id);
            if (servico == null)
            {
                return HttpNotFound();
            }
            return View(servico);
        }

        //
        // GET: /Servico/Create

        public ActionResult Create()
        {

            ViewBag.EmpresaId = IdEmpresaLogada();
            ViewBag.AuditoriaId = new SelectList(db.Auditoria, "Id", "UltimaOperacao");
            return View();
        }

        //
        // POST: /Servico/Create

        [HttpPost]
        public ActionResult Create(Servico servico)
        {
            

            if (ModelState.IsValid)
            {
                Auditoria auditoria = new Auditoria();
                auditoria.DataAtualizacao = DateTime.Today;
                auditoria.DataCadastro = DateTime.Today;
                auditoria.UserId = WebSecurity.CurrentUserId;
                auditoria.UltimaOperacao = "Inclusao";
                servico.Auditoria = auditoria;
                servico.EmpresaId = IdEmpresaLogada();
                db.Servicos.Add(servico);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EmpresaId = new SelectList(db.Empresas, "Id", "RazaoSocial", servico.EmpresaId);
            ViewBag.AuditoriaId = new SelectList(db.Auditoria, "Id", "UltimaOperacao", servico.AuditoriaId);
            return View(servico);
        }

        //
        // GET: /Servico/Edit/5

        public ActionResult Edit(int id = 0)
        {
            idEmpresa = IdEmpresaLogada();
            Servico servico = db.Servicos.Where( d => d.Id==id && d.EmpresaId == idEmpresa).First();
            if (servico == null)
            {
                return HttpNotFound();
            }
            ViewBag.EmpresaId = new SelectList(db.Empresas, "Id", "RazaoSocial", servico.EmpresaId);
            ViewBag.AuditoriaId = new SelectList(db.Auditoria, "Id", "UltimaOperacao", servico.AuditoriaId);
            return View(servico);
        }

        //
        // POST: /Servico/Edit/5

        [HttpPost]
        public ActionResult Edit(Servico servico)
        {
            if (ModelState.IsValid)
            {
                idEmpresa = IdEmpresaLogada();
                servico.EmpresaId = idEmpresa;
                servico.Empresa = db.Empresas.Where(e => e.Id == idEmpresa).FirstOrDefault();
                servico.Auditoria = db.Auditoria.Find(servico.AuditoriaId);
                servico.Auditoria.UltimaOperacao = "Alteração";
                db.Entry(servico).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AuditoriaId = new SelectList(db.Auditoria, "Id", "UltimaOperacao", servico.AuditoriaId);
            return View(servico);
        }

        //
        // GET: /Servico/Delete/5

        public ActionResult Delete(int id = 0)
        {
            int idEmpresa = IdEmpresaLogada();
            Servico servico = db.Servicos.Where(d => d.Id == id && d.EmpresaId == idEmpresa).First();
            if (servico == null)
            {
                return HttpNotFound();
            }
            return View(servico);
        }

        //
        // POST: /Servico/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            int idEmpresa = IdEmpresaLogada();
            Servico servico = db.Servicos.Where(d => d.Id == id && d.EmpresaId == idEmpresa).First();
            db.Servicos.Remove(servico);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}