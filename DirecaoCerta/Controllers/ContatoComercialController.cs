﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DirecaoCerta.Controllers
{
    public class ContatoComercialController : AplicationController
    {
        //
        // GET: /ContatoComercial/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Enviar(string nome, string email, string mensagem)
        {
            new MailController().EmailContatoComercial(nome, email, mensagem).DeliverAsync();

            ViewBag.Mensagem = "Seu e-mail foi enviado com sucesso. Em breve entraremos em contato.";

            //return RedirectToAction("Index", "Home");
            return View();
        }

    }
}
