﻿using DirecaoCerta.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace DirecaoCerta.Controllers
{
    public class AplicationController : Controller
    {
 
        public Empresa EmpresaSessao(){
            
           return  (Empresa)Session[" Empresa "];

        }

        public int IdEmpresaLogada()
        {

            return (Session[" Empresa "] == null) ? 0 : ((Empresa)Session[" Empresa "]).Id;

        }


        public void SalvarEmpresaSessao(Empresa empresaLogada)
        {
            Session[" Empresa "] = empresaLogada;
        }
         
    }
}
