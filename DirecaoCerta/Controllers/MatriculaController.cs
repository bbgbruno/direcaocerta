﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DirecaoCerta.Models;
using WebMatrix.WebData;

namespace DirecaoCerta.Controllers
{
    [Authorize]
    public class MatriculaController : AplicationController
    {
        private ContextoDeDados db = new ContextoDeDados();

        //
        // GET: /Matricula/

        public ActionResult Index()
        {
            var matriculas = db.Matriculas.Include(m => m.Auditoria).Include(m => m.aluno);
            return View(matriculas.ToList());
        }

        //
        // GET: /Matricula/Details/5

        public ActionResult Details(int id = 0)
        {
            Matricula matricula = db.Matriculas.Find(id);
            if (matricula == null)
            {
                return HttpNotFound();
            }
            return View(matricula);
        }

        //
        // GET: /Matricula/Create

        public ActionResult Create()
        {
            ViewBag.AlunoId = new SelectList(db.Pessoas, "Id", "Nome");
            return View();
        }

        //
        // POST: /Matricula/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Matricula matricula)
        {
            if (ModelState.IsValid)
            {
                Auditoria auditoria = new Auditoria();
                auditoria.DataAtualizacao = DateTime.Today;
                auditoria.DataCadastro = DateTime.Today;
                auditoria.UserId = WebSecurity.CurrentUserId;
                auditoria.UltimaOperacao = "Inclusao";
                matricula.Auditoria = auditoria;
                matricula.DataMatricula = DateTime.Now;
                matricula.Ativo = 1;
                
                db.Matriculas.Add(matricula);

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AlunoId = new SelectList(db.Pessoas, "Id", "Nome", matricula.AlunoId);
            return View(matricula);
        }

        //
        // GET: /Matricula/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Matricula matricula = db.Matriculas.Find(id);
            if (matricula == null)
            {
                return HttpNotFound();
            }
            ViewBag.AuditoriaId = new SelectList(db.Auditoria, "Id", "UltimaOperacao", matricula.AuditoriaId);
            ViewBag.AlunoId = new SelectList(db.Pessoas, "Id", "Email", matricula.AlunoId);
            return View(matricula);
        }

        //
        // POST: /Matricula/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Matricula matricula)
        {
            if (ModelState.IsValid)
            {
                db.Entry(matricula).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AuditoriaId = new SelectList(db.Auditoria, "Id", "UltimaOperacao", matricula.AuditoriaId);
            ViewBag.AlunoId = new SelectList(db.Pessoas, "Id", "Email", matricula.AlunoId);
            return View(matricula);
        }

        //
        // GET: /Matricula/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Matricula matricula = db.Matriculas.Find(id);
            if (matricula == null)
            {
                return HttpNotFound();
            }
            return View(matricula);
        }

        //
        // POST: /Matricula/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Matricula matricula = db.Matriculas.Find(id);
            db.Matriculas.Remove(matricula);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}