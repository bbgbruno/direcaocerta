﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DirecaoCerta.Models;
using DirecaoCerta.Filters;
using WebMatrix.WebData;

namespace DirecaoCerta.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class AlunoController : AplicationController
    {
        private ContextoDeDados db = new ContextoDeDados();
        private int idEmpresa;

        //
        // GET: /Aluno/

        public ActionResult Index()
        {
            idEmpresa = IdEmpresaLogada();
            var pessoas = db.Alunos.Include(a => a.Empresa).Include(a => a.Endereco).Where(d => d.EmpresaId == idEmpresa);
            return View(pessoas.ToList());
        }

        //
        // GET: /Aluno/Details/5

        public ActionResult Details(int id = 0)
        {
            idEmpresa = IdEmpresaLogada();
            Aluno aluno = db.Alunos.Where(d => d.Id == id && d.EmpresaId == idEmpresa).First();
            if (aluno == null)
            {
                return HttpNotFound();
            }
            return View(aluno);
        }

        //
        // GET: /Aluno/Create

        public ActionResult Create()
        {
            Aluno model = new Aluno();
            model.Endereco = new Endereco();
            return View(model);
        }

        //
        // POST: /Aluno/Create

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(Aluno aluno, Endereco endereco)
        {
            if (ModelState.IsValid)
            {
                Auditoria auditoria = new Auditoria();
                auditoria.DataAtualizacao = DateTime.Today;
                auditoria.DataCadastro = DateTime.Today;
                auditoria.UserId = WebSecurity.CurrentUserId;
                auditoria.UltimaOperacao = "Inclusao";
                aluno.Auditoria = auditoria;
                aluno.EmpresaId = IdEmpresaLogada();
                aluno.Endereco = endereco;
                db.Alunos.Add(aluno);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(aluno);
        }

        //
        // GET: /Aluno/Edit/5

        public ActionResult Edit(int id = 0)
        {
            idEmpresa = IdEmpresaLogada();
            Aluno aluno = db.Alunos.Where(d => d.Id == id && d.EmpresaId == idEmpresa).First();
            if (aluno == null)
            {
                return HttpNotFound();
            }

            return View(aluno);
        }

        //
        // POST: /Aluno/Edit/5

        [HttpPost]
       // [ValidateAntiForgeryToken]
        public ActionResult Edit(Aluno aluno)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aluno).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aluno);
        }

        //
        // GET: /Aluno/Delete/5

        public ActionResult Delete(int id = 0)
        {
            idEmpresa = IdEmpresaLogada();
            Aluno aluno = db.Alunos.Where(d => d.Id == id && d.EmpresaId == idEmpresa).First();
            if (aluno == null)
            {
                return HttpNotFound();
            }
            return View(aluno);
        }

        //
        // POST: /Aluno/Delete/5

        [HttpPost, ActionName("Delete")]
       // [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            idEmpresa = IdEmpresaLogada();
            Aluno aluno = db.Alunos.Where(d => d.Id == id && d.EmpresaId == idEmpresa).First();
            if (aluno == null)
            {
                return HttpNotFound();
            }
            return View(aluno);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}