﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DirecaoCerta.Filters;
using DirecaoCerta.Models;
using WebMatrix.WebData;
using System.Text;

namespace DirecaoCerta.Controllers
{
    [InitializeSimpleMembership]
    public class EmpresaController : AplicationController
    {
        private ContextoDeDados db = new ContextoDeDados();
        public int idEmpresa;
       
        //
        // GET: /Empresa/

        public ActionResult Index()
        {

            idEmpresa = IdEmpresaLogada(); 
            return View(db.Empresas.Where(d => d.Id == idEmpresa).ToList());
        }

        //
        // GET: /Empresa/Details/5

        public ActionResult Details()
        {
            idEmpresa = IdEmpresaLogada(); 
            Empresa empresa = db.Empresas.Find(idEmpresa);
            if (empresa == null)
            {
                return HttpNotFound();
            }
            return View(empresa);
        }

        //
        // GET: /Empresa/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Empresa/Create

        public string CreatePassword(int tamanho)
        {
            const string SenhaCaracteresValidos = "abcdefghijklmnopqrstuvwxyz1234567890@#!?";
            int valormaximo = SenhaCaracteresValidos.Length;
            Random random = new Random(DateTime.Now.Millisecond);
            StringBuilder senha = new StringBuilder(tamanho);
            for (int indice = 0; indice < tamanho; indice++)
                senha.Append(SenhaCaracteresValidos[random.Next(0, valormaximo)]);
            return senha.ToString();
        }

        public static string soNumero(string toNormalize)
        {
            List<char> numbers = new List<char>("0123456789");
            StringBuilder toReturn = new StringBuilder(toNormalize.Length);
            CharEnumerator enumerator = toNormalize.GetEnumerator();

            while (enumerator.MoveNext())
            {
                if (numbers.Contains(enumerator.Current))
                    toReturn.Append(enumerator.Current);
            }

            return toReturn.ToString();
        }

        [HttpPost]
        public ActionResult Create(Empresa empresa)
        {
            if (ModelState.IsValid)
            {
                RegisterModel novoUsuario = new RegisterModel();
                empresa.CNPJ = soNumero(empresa.CNPJ);
                novoUsuario.UserName = empresa.Usuario;
                novoUsuario.Password = "master";
                //CreatePassword(8);
                db.Empresas.Add(empresa);
                
                WebSecurity.CreateUserAndAccount(novoUsuario.UserName, novoUsuario.Password); //gerar senha automatica 
                empresa.UserId = WebSecurity.GetUserId(novoUsuario.UserName);   //Mandar email com a senha para o usuario

                String mensagem = "Usuário : " + novoUsuario.UserName + " / Senha : " + novoUsuario.Password;
                new MailController().CadastroNovaEmpresa(empresa, novoUsuario.Password, mensagem).Deliver();
                ViewBag.Mensagem = "Um e-mail com usuário e senha foi enviado para " + empresa.Email;

                db.SaveChanges();

                return RedirectToAction("Index");
            }
 
            return View(empresa);
        }

        //
        // GET: /Empresa/Edit/5

        public ActionResult Edit()
        {
            idEmpresa = IdEmpresaLogada(); 
            Empresa empresa = db.Empresas.Find(idEmpresa);
            if (empresa == null)
            {
                return HttpNotFound();
            }
            return View(empresa);
        }

        //
        // POST: /Empresa/Edit/5

        [HttpPost]
        public ActionResult Edit(Empresa empresa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(empresa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(empresa);
        }

        //
        // GET: /Empresa/Delete/5

        public ActionResult Delete()
        {
            idEmpresa = IdEmpresaLogada(); 
            Empresa empresa = db.Empresas.Find(idEmpresa);
            if (empresa == null)
            {
                return HttpNotFound();
            }
            return View(empresa);
        }

        //
        // POST: /Empresa/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            idEmpresa = IdEmpresaLogada(); 
            Empresa empresa = db.Empresas.Find(idEmpresa);
            db.Empresas.Remove(empresa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}