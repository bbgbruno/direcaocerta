﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DirecaoCerta.Models;
using WebMatrix.WebData;

namespace DirecaoCerta.Controllers
{
    public class VeiculoController : AplicationController
    {
        private ContextoDeDados db = new ContextoDeDados();
        
        
        //
        // GET: /Veiculos/

        public ActionResult Index()
        {
            int idEmpresa = IdEmpresaLogada();
            var veiculos = db.Veiculos.Include(v => v.Empresa).Include(v => v.Auditoria).Where(d => d.EmpresaId == idEmpresa); 
            return View(veiculos.ToList()); 
        }

        //
        // GET: /Veiculos/Details/5

        public ActionResult Details(int id = 0)
        {
            Veiculo veiculo = db.Veiculos.Find(id);
            if (veiculo == null)
            {
                return HttpNotFound();
            }
            return View(veiculo);
        }

        //
        // GET: /Veiculos/Create

        public ActionResult Create()
        {
            ViewBag.EmpresaId = IdEmpresaLogada();
            return View();
        }

        //
        // POST: /Veiculos/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Veiculo veiculo)
        {
            if (ModelState.IsValid)
            {
                Auditoria auditoria = new Auditoria();
                auditoria.DataAtualizacao = DateTime.Today;
                auditoria.DataCadastro = DateTime.Today;
                auditoria.UserId = WebSecurity.CurrentUserId;
                auditoria.UltimaOperacao = "Inclusao";
                veiculo.Auditoria = auditoria;
                veiculo.EmpresaId = IdEmpresaLogada();
                db.Veiculos.Add(veiculo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(veiculo);
        }

        //
        // GET: /Veiculos/Edit/5

        public ActionResult Edit(int id = 0)
        {
            int idEmpresa = IdEmpresaLogada();
            Veiculo veiculo = db.Veiculos.Where(d => d.Id == id && d.EmpresaId == idEmpresa).First();
            
            if (veiculo == null)
            {
                return HttpNotFound();
            }
            return View(veiculo);
        }

        //
        // POST: /Veiculos/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Veiculo veiculo)
        {
            if (ModelState.IsValid)
            {
                int idEmpresa = IdEmpresaLogada();
                veiculo.EmpresaId = idEmpresa;
                veiculo.Auditoria = db.Auditoria.Find(veiculo.AuditoriaId);
                veiculo.Auditoria.UltimaOperacao = "Alteração";
                db.Entry(veiculo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(veiculo);
        }

        //
        // GET: /Veiculos/Delete/5

        public ActionResult Delete(int id = 0)
        {
            int idEmpresa = IdEmpresaLogada();
            Veiculo veiculo = db.Veiculos.Where(d => d.Id == id && d.EmpresaId == idEmpresa).First();
            if (veiculo == null)
            {
                return HttpNotFound();
            }
            return View(veiculo);
        }

        //
        // POST: /Veiculos/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            int idEmpresa = IdEmpresaLogada();
            Veiculo veiculo = db.Veiculos.Where(d => d.Id == id && d.EmpresaId == idEmpresa).First();
            db.Veiculos.Remove(veiculo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}