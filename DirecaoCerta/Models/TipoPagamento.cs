﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DirecaoCerta.Models
{
    [Table("TiposPagamento")]
    public class TipoPagamento
    {
        public int Id { get; set; }
        public int EmpresaId { get; set; }
        public int AuditoriaId { get; set; }
        
        [Description("Nome do Tipo de Pgto.")]
        public string Descricao { get; set; }
        public virtual Empresa Empresa { get; set; }
        public virtual Auditoria Auditoria { get; set; }
        
    }
}