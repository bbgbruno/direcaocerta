﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DirecaoCerta.Models
{
    public class Matricula
    {
        public int Id { get; set; }
        public int AlunoId { get; set; }
        public int AuditoriaId { get; set; }
        
        public float ValorContrato { get; set; }
        public DateTime DataMatricula { get; set; }
        public DateTime DataConclusao { get; set; }
        public int Ativo { get; set; }
                
        public virtual Auditoria Auditoria { get; set; }
        public virtual Aluno aluno { get; set; }
        
        public virtual ICollection<Servico> Servicos { get; set; }
        
    }
}