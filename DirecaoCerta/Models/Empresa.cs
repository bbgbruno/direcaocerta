﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DirecaoCerta.Models
{
    [Table("Empresas")]
    public class Empresa
    {
        public int Id { get; set; }

        [DisplayName("Razão Social")]
        [Required(ErrorMessage = "Informe a razão social!")]
        public string RazaoSocial { get; set; }
        
        public string CNPJ { get; set; }

        [DisplayName("Responsável")]
        [Required(ErrorMessage = "Informe o responsável!")]
        public string Responsavel { get; set; }

        [NotMappedAttribute]
        public string Usuario { get; set; }

        [DisplayName("E-mail")]
        public string Email { get; set; }
        
        public int UserId { get; set; }

    }
}