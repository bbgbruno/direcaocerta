﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DirecaoCerta.Models
{
    [Table("Servicos")]
    public class Servico
    {
        public int Id { get; set; }
        public int AuditoriaId { get; set; }
        public int EmpresaId { get; set; }

        [DisplayName("Nome do Serviço")]
        [Required(ErrorMessage = "Informe a descrição do serviço!")]
        [MinLength(5)]
        public string Descricao { get; set; }

        [DisplayName("Valor")]
        [Required(ErrorMessage = "Informe o valor do serviço!")]
        public float Valor { get; set; }
        public virtual Empresa Empresa { get; set; }
        public virtual Auditoria Auditoria { get; set; }
     

    }
}