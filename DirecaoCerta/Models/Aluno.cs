﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DirecaoCerta.Models
{
    [Table("Alunos")]
    public class Aluno : Pessoa
    {
        public string CPF { get; set; }
        public string Residencial { get; set; }
        public string Celular { get; set; }
        public string Contato { get; set; }


        [DisplayName("Data de Aceite")]
        [Required(ErrorMessage = "Informe a data de aceite!")]
        public DateTime Data_Aceite { get; set; }
        
        public virtual ICollection<Matricula> Matriculas { get; set; }
  
    }
}