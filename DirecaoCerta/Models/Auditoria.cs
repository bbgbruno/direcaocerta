﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DirecaoCerta.Models
{
    [Table("Auditoria")]
    public class Auditoria
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        [DisplayName("Última operação")]
        public string UltimaOperacao { get; set; }

        [DisplayName("Data de Cadastro")]
        public DateTime DataCadastro { get; set; }

        [DisplayName("Data de Atualização")]
        public DateTime DataAtualizacao { get; set; }

    }
}