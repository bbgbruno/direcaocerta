﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DirecaoCerta.Models
{
    [Table("Pessoas")]
    public class Pessoa
    {
        public int Id { get; set; }
        public int EmpresaId { get; set; }
        public int AuditoriaId { get; set; }
        
        [DisplayName("E-mail")]
        public string Email { get; set; }

        [DisplayName("Nome")]
        [Required(ErrorMessage = "Informe a descrição")]
        public string Nome { get; set; }
        public virtual Empresa Empresa { get; set; }
        public virtual Endereco Endereco { get; set; }
        public virtual Auditoria Auditoria { get; set; }

    }
}