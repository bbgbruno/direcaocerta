﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DirecaoCerta.Models
{
    [Table("Veiculos")]
    public class Veiculo
    {
        public int Id { get; set; }
        public int EmpresaId { get; set; }
        public int AuditoriaId { get; set; }
        public string Renavan { get; set; }
        public string modelo { get; set; }
        public string Placa { get; set; }
        public string Ano { get; set; }
        public string Cor { get; set; }
        public virtual Empresa Empresa { get; set; }
        public virtual Auditoria Auditoria { get; set; }
    }
}