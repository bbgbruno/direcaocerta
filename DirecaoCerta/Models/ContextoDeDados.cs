﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DirecaoCerta.Models 
{
    public class ContextoDeDados : DbContext
    {
        public DbSet<Aluno> Alunos {get; set;}
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Pessoa> Pessoas {get; set;}
        public DbSet<Endereco> Enderecos {get; set;}
        public DbSet<Auditoria> Auditoria { get; set; }
        public DbSet<Servico> Servicos { get; set; }
        public DbSet<TipoPagamento> TipoPagamentos { get; set; }
        public DbSet<Veiculo> Veiculos { get; set; }
        public DbSet<Matricula> Matriculas { get; set; }


    }
}