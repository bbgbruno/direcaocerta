﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DirecaoCerta.Models
{
    [Table("Enderecos")]
    public class Endereco
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Informe o Logradouro do endereço!")]
        public string Logradouro { get; set; }

        [Required(ErrorMessage = "Informe a Cidade do endereço!")]
        public string Cidade { get; set; }

        [Required(ErrorMessage = "Informe o Bairro do endereço!")]
        public string Bairro { get; set; }

        [Required(ErrorMessage = "Informe o CEP do endereço!")]
        public string CEP { get; set; }

        [DisplayName("Número")]
        [Required(ErrorMessage = "Informe o número do endereço!")]
        public int Numero { get; set; }
    }
}